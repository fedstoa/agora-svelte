import linkifyHtml from 'linkifyjs/html';
const wikireg = /(.*?)\[\[(.*?)\]\](.*?)/g;
const urlreg = /https?:\/\/|(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+~#?&//=]*)/g
const replacer = (match, p1, p2, p3, offset, string) => {
    let link = p2.replace(/ /g,"-").toLowerCase()
    return `${p1} [[<a href='/node/${link}'>${p2}</a>]] ${p3}`
}
const urlReplacer = (match, p1, p2, p3, offset, string) => {
    console.log(match,"-",p1,"-",p2,"-",p3)
    return `<a href='${match}'>${match}</a>`
}

export const replaceWiki = (str) => {
    str = str.replace(wikireg, replacer)
    return linkifyHtml(str)
}
